# hermes_idaf



## Description
IDF, ARF and IDAF module


## Installation

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/hermes_idaf.git
```

Ou

```
pip install git@gricad-gitlab.univ-grenoble-alpes.fr:mnemosyne/hermes_idaf.git
```

## Test install

Get install directory


```
pip show hades_stats
```

Use pytest on hades_stats path

```
pytest --doctest-modules /path_wherre_pip_install/site-packages/hermes_idaf
```

## Usage




```

```

## License
GNU GPL

