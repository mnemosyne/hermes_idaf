"""
IDF
"""

##====================================================================================================================##
##                                Importation des packages                                                            ##
##====================================================================================================================##

#package python
import os
import argparse
import doctest
from collections.abc import Iterable
from numbers import Number


import numpy
from matplotlib import pyplot
import scipy.optimize
import scipy.stats
import pandas
import dill # pickle



#packages perso
from hades_stats import Distribution, Sample, FastLinearRegression, FitDist, SampleDistr#, Multi_SampleDistr
from hades_stats import LMOM, MOM, MLE

from japet_misc import show_all, close_all, get_colormap, plot_multimarkers, plot_one_one

##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##

KOUT_NAME = "koutsoyiannis"
SIMSCALING_NAME = "simple_scaling"

#~ MOM_METHOD     = "moment"
KRUSKAL = "kruskal"

#~ LMOM_METHOD = "lmoment"
#~ MLE_METHOD  = "mle"

ETA_NM = "eta"
THETA_NM = "theta"


##======================================================================================================================##
##                RANDOM GENERATION OF SCALING DATA                                                                     ##
##======================================================================================================================##

def random_sca_data(nval=20, scales=None, scaling_expr=None, scaling_factors=None, distribution=None, seed=1):
    """
    Randomlu generated scaling data
    >>> ex_data = random_sca_data(nval = 5, scales = [1,2,12,24], seed = 0)
    >>> ex_data
              1          2          12        24
    0  52.023072  30.336462   7.938888  8.704957
    1  67.685996  18.249808   9.155502  4.546241
    2  20.309984  33.256129   6.420217  9.024606
    3  47.336340  30.320158   7.683030  5.294645
    4  40.534874  29.959588  11.849711  5.203372
    >>> print(ex_data.distribution)
    name     genextreme
    a               -10
    b               inf
    c              -0.1
    loc              50
    scale            15
    
    >>> print(ex_data.scaling_expr)
    SIMPLE_SCALING
    eta   -0.8
    dtype: float64
    """
    if scales is None:
        scales = numpy.arange(1, 12)
    
    if scaling_expr is None and scaling_factors is None:
        scaling_expr = ScalingExpr(scaling="s", eta=-0.8)

    if distribution is None:
        distribution = Distribution("gev", c=-0.1, loc=50, scale=15)

    # ~ print(scaling_factors)
    res = IDF({}, scaling_expr=scaling_expr, scaling_factors=scaling_factors, distribution=distribution, columns=scales)        
    
    # ~ print(res.get_scaling_factors())
    distrs = res.scaled_distributions()
    for iseed, isca in enumerate(res.columns):
        dist = distrs.loc[isca]
        vals = dist.random(nval, seed=seed + iseed +1)
        res[isca] = vals
            
    return res



##======================================================================================================================##
##                                SCALING EXPRS                                                                         ##
##======================================================================================================================##

def koutsoyiannis_scales(scales, theta):
    """
    >>> (koutsoyiannis_scales(numpy.arange(10), 1) == numpy.arange(1,11)).all()
    True
    """
    res = scales + theta
    return res

def simple_scaling(scales, eta, d0=1, idx0=None):
    """
    Function doc
    >>> xs = numpy.arange(1.,5., dtype = float)**2
    >>> all(simple_scaling(xs,0) == numpy.array([1., 1., 1., 1.]))
    True
    >>> all(simple_scaling(xs,1) == numpy.array([ 1.,  4.,  9., 16.]))
    True
    >>> all(simple_scaling(xs,0.5) == numpy.array([1., 2., 3., 4.]))
    True
    """
    if d0:
        assert isinstance(d0, Number)
        assert idx0 is None, "please choose reference duration or reference duration index"
    elif idx0:
        d0 = scales[idx0]
    else:
        d0 = scales[0]
        
    res = (scales /  d0) ** eta
    return res
    
def multi_simple_scaling(scales, etas):
    """
    Function doc
    >>> r=multi_simple_scaling([2,4,8,16,32,64,128], [0.9,0.5,0.1,0.1,0.5,0.9])
    >>> r.round(2)
    2.0      1.00
    4.0      1.87
    8.0      2.64
    16.0     2.83
    32.0     3.03
    64.0     4.29
    128.0    8.00
    dtype: float64
    >>> (r.iloc[:-1].values / r.iloc[1:].values).round(2)
    array([0.54, 0.71, 0.93, 0.93, 0.71, 0.54])
    """
    assert sorted(scales) == list(scales)
    nstep = len(etas)
    assert len(scales) == nstep + 1
    scales = numpy.array(scales, dtype=float)
    dref = scales[0]
    norm_scales = scales / dref
    res = [1]
    for i in range(nstep):
        d0 = norm_scales[i]
        d1 = norm_scales[i+1]
        eta = etas[i]
        ratio = d1 / d0
        factor1 = ratio ** eta
        factor = factor1 * res[-1]
        # ~ print(ratio, factor1, factor)
        res.append(factor)
        
    res = pandas.Series(res, index=scales)
    return res
        
        
    

def scaling_name(scaling):
    """
    >>> scaling_name("k")
    'koutsoyiannis'
    >>> scaling_name("s")
    'simple_scaling'
    """
    scaling = scaling.lower()
    if scaling in ("simple", "s", SIMSCALING_NAME):
        res = SIMSCALING_NAME
    elif scaling in ("k", "kout", KOUT_NAME):
        res = KOUT_NAME
    else:
        raise ValueError(f"{scaling} not implemented")
    
    return res
    
def init_param_scaling(scaling, **kwargs):
    """
    >>> init_param_scaling(SIMSCALING_NAME)
    eta    0.5
    dtype: float64
    >>> init_param_scaling(SIMSCALING_NAME, eta0 = 0.2)
    eta    0.2
    dtype: float64
    >>> init_param_scaling(KOUT_NAME, theta0 = 0)
    eta      0.5
    theta    0.0
    dtype: float64
    >>> init_param_scaling(KOUT_NAME, eta0 = 0.2, theta0 = - 0.5)
    eta      0.2
    theta   -0.5
    dtype: float64
    """
    if "eta0" in kwargs:
        eta0 = kwargs.pop("eta0")
    else:
        eta0 = 0.5
    
    if "theta0" in kwargs:
        theta0 = kwargs.pop("theta0")
    else:
        theta0 = 0
        
    assert not kwargs
    
    if scaling == SIMSCALING_NAME:
        param0 = [eta0]
        pnames = [ETA_NM]
    elif scaling == KOUT_NAME:
        param0 = [eta0, theta0]
        pnames = [ETA_NM, THETA_NM]
    else:
        raise ValueError("pb code")
    
    res = pandas.Series(param0, index=pnames)
    
    return res
    
def npar_scaling(scaling):
    """
    Return the number of parameters of the scaling expr
    >>> npar_scaling(SIMSCALING_NAME)
    1
    >>> npar_scaling(KOUT_NAME)
    2
    """
    if scaling == SIMSCALING_NAME:
        res = 1
    elif scaling == KOUT_NAME:
        res = 2
    else:
        raise ValueError("pb code")
    
    return res

class ScalingExpr:
    """ Class doc
    >>> ss = ScalingExpr(scaling ="s", eta = 0.5)
    >>> ss.eta
    0.5
    >>> hasattr(ss, THETA_NM)
    False
    >>> ss(4)
    2.0
    >>> ss1 = ss.new_params(eta = 1.0)
    >>> ss.eta
    0.5
    >>> ss1.eta
    1.0
    >>> ss1(4)
    4.0
    >>> ss.scaling
    'simple_scaling'
    >>> del ss, ss1
    
    >>> ks = ScalingExpr(scaling ="k", eta = 0.5, theta = 1)
    >>> ks.eta
    0.5
    >>> hasattr(ks, THETA_NM)
    True
    >>> ks.theta
    1.0
    >>> ks(3)
    2.0
    >>> ks1 = ks.new_params(eta = 1.0, theta = 2)
    >>> ks.eta
    0.5
    >>> ks1.eta
    1.0
    >>> ks.theta
    1.0
    >>> ks1.theta
    2.0
    >>> ks1(4)
    6.0
    
    >>> ks.scaling
    'koutsoyiannis'
    
    >>> fig = pyplot.figure()
    >>> l = ks1.plot(numpy.arange(10))
    >>> fig.show()
    >>> pyplot.close()
    """
    
    def __init__(self, *arg, scaling=SIMSCALING_NAME, **kwargs):
        """ Class initialiser """
        #~ print(arg, kwargs)
        self.scaling = scaling_name(scaling=scaling)

        
        if arg:
            arg = list(arg)
            eta = arg.pop(0)
            if self.scaling == KOUT_NAME:
                theta = arg.pop(0)
            
        elif kwargs:
            eta = kwargs.pop(ETA_NM)
            if self.scaling == KOUT_NAME:
                theta = kwargs.pop(THETA_NM)
        else:
            raise ValueError(f"not sufficient args : {arg}, {kwargs}")
            
        self.eta = float(eta)
        if self.scaling == KOUT_NAME:
            self.theta = float(theta)

        
        assert not kwargs, f"Dont know what to do with {kwargs}"
        assert not arg, f"Dont know what to do with {arg}"

    def __str__(self):
        """
        Object representation
        >>> print(ScalingExpr(scaling = "k", eta = 0.5, theta = 1))
        KOUTSOYIANNIS
        eta      0.5
        theta    1.0
        dtype: float64
        >>> print(ScalingExpr(scaling = "s", eta = 0.5))
        SIMPLE_SCALING
        eta    0.5
        dtype: float64
        """
        hd = self.scaling.upper()
        df = self.params()
        
        res = "\n".join([hd, str(df)])
        return res
        
    def params(self):
        """Return a pandas.Series containing parameters"""
        cols = [k for k in (ETA_NM, THETA_NM) if hasattr(self, k)]
        
        res = {k : getattr(self, k) for k in cols}
        res = pandas.Series(res, index=cols)
        return res
        
    def __eq__(self, other):
        """
        Function doc
        >>> s1 = ScalingExpr(scaling = "s", eta = 0.5)
        >>> s2 = ScalingExpr(scaling = "s", eta = 0.5)
        >>> s1 == s2
        True
        
        >>> s1 = ScalingExpr(scaling = "k", eta = 0.5, theta = 1)
        >>> s1 == s2
        False
        >>> s2 = ScalingExpr(scaling = "k", eta = 0.5, theta = 1)
        >>> s1 == s2
        True
        
        """
        _num_approx = 1e-6
        if isinstance(other, ScalingExpr) and other.scaling == self.scaling:
            d1 = self.params()
            d2 = other.params()
            dif = (d1 - d2).abs()
            # ~ print(dif)
            res = (dif <= _num_approx).all()
        else:
            res = False
            
        return res

    def new_params(self, **kwargs):
        """Return a new expr with different parameters"""
        res = ScalingExpr(scaling=self.scaling, **kwargs)
        return res

    def __call__(self, scales):
        """Return scaled ratio for given scales"""
        if self.scaling == KOUT_NAME:
            scales = koutsoyiannis_scales(scales=scales, theta=self.theta)
        
        res = simple_scaling(scales=scales, eta=self.eta)
        return res
        
    def plot(self, scales, **kwargs):
        """Plot the scaling ratios for the given scales"""
        y = self(scales)
        res = pyplot.plot(scales, y, **kwargs)
        return res

##======================================================================================================================##
##                SCALING DATA                                                                                          ##
##======================================================================================================================##

class ScalingData(pandas.DataFrame):
    """ Class Scaling Data
    >>> rdm_data = random_sca_data(nval = 10, seed = 0)
    >>> sca_data = ScalingData(rdm_data)
    >>> isinstance(sca_data, ScalingData)
    True
    
    See IDF doctest for more examples
    """
    _metadata = ["scaling_expr", "distribution", "scaling_factors"]
    #~ added_property = 1  # This will be passed to copies #not necessary
    
    def __init__(self, data, scaling_expr=None, distribution=None, scaling_factors=None, **kwargs):
        """ Class initialiser """
        pandas.DataFrame.__init__(self, data, **kwargs)
        assert (self.columns.sort_values(ascending=True) == self.columns).all(), "please sort columns"
        
        assert isinstance(distribution, Distribution) or distribution is None
        self.distribution = distribution
        
        
        if isinstance(scaling_expr, ScalingExpr):
            assert scaling_factors is None
        else:
            assert scaling_expr is None
            
        if isinstance(scaling_factors, pandas.Series):
            assert scaling_expr is None
            assert (scaling_factors.index == self.columns).all()
        else:
            assert scaling_factors is None
            
        
        self.scaling_expr = scaling_expr
        self.scaling_factors = scaling_factors
        
    
    @property
    def _constructor(self):
        """For returning ScalingData instead of DataFrame"""
        return ScalingData
        
        
    def compute_moments_vs_scales(self, moment_order, log=True):
        """
        Input moment order
        Compute the given moment for the range of scales
        See main doctest
        """
        moments = []
        for iscale in self.columns:
            isample = self[iscale]
            isample = isample.dropna()
            isample = Sample(isample)
            imoment = isample.compute_moment(moment_order, kind_of_moment="raw")
            moments.append(imoment)
            
        moments = numpy.array(moments)
        assert (moments > 0).all(), f"moment computation not possible {moments}"
        
        scales = self.columns
        
        if log:
            scales = numpy.log10(scales)
            moments = numpy.log10(moments)
        
        res = pandas.Series(moments, index=scales)
        
        return res

    def linear_regresion_logmoments_vs_logscales(self, moment_order):
        """Compute a linear regression on log scales"""
        df = self.compute_moments_vs_scales(moment_order=moment_order, log=True)
        x = df.index
        y = df.values
        res = FastLinearRegression(x=x, y=y)

        return res

    def plot_moments_vs_scales(self, moment_order, log=True, **kwargs):
        """Plot moment vs scales : default log space"""
        argdic = {'ls' : "none", 'marker' : "s"}
        argdic.update(kwargs)
        
        df = self.compute_moments_vs_scales(moment_order=moment_order, log=log)
        x = df.index
        y = df.values
        res = pyplot.plot(x, y, **argdic)[0]

        return res

    def compute_moment_scaling_function(self, moments):
        """Compute k(q) vs q"""
        #~ assert isinstance(moments, Iterable), 'exp {0} got {1.__class__} : {1}'.format("list", moments)
        res = [self.linear_regresion_logmoments_vs_logscales(moment_order=moment)["gradient"] for moment in moments]
        res = numpy.array(res, dtype=float)
        return res

    def fit_linear_regression_k_vs_kq(self, moments):
        """Linear regression on k(q) vs q"""
        k_qs = self.compute_moment_scaling_function(moments=moments)
        res = FastLinearRegression(x=moments, y=k_qs)
        return res

    def plot_moment_scaling_function(self, moments, **kwargs):
        """Plot k(q) vs q"""
        argdic = dict(ls="none", marker="o")
        argdic.update(kwargs)
        
        k_q = self.compute_moment_scaling_function(moments=moments)
        res = pyplot.plot(moments, k_q, **argdic)[0]

        return res

    def plot_simple_scaling(self, moments, method="q1"):
        """Plot the fitted simple scaling k(q) vs q"""
        moments = numpy.array(moments, dtype=float)
        if method == "q1":
            k_q1 = self.compute_moment_scaling_function(moments=[1])
            k_qs = k_q1 * moments
            res = pyplot.plot(moments, k_qs)[0]
        elif method == "lr":
            xlim = (moments[0], moments[-1])
            res = self.lr_kq_vs_q.lr_expr.plot(xlims=xlim)
        else:
            assert 0, f'pb input expect q1 ou lr got {method.__class__} : {method}'

        pyplot.xlabel("q, Order of the moment (-)")
        pyplot.ylabel("K(q) : Moment scaling function")

        return res
        
    def get_scaling_factors(self, scaling_expr=None, scaling_factors=None):
        """Return the scaling factors for each scales"""
        if scaling_expr is None:
            scaling_expr = self.scaling_expr
        else:
            assert isinstance(scaling_expr, ScalingExpr), f"exp ScalingExpr got {scaling_expr} : {scaling_expr.__class__}"
        
        if scaling_factors is None:
            scaling_factors = self.scaling_factors
        else:
            assert isinstance(scaling_factors, pandas.Series), f"exp Series got {scaling_factors}: type={type(scaling_factors)}"
            assert (scaling_factors.index == self.columns).all()
        
        if scaling_expr:
            assert scaling_factors is None
            scales = numpy.array(self.columns, dtype=float)
            factors = scaling_expr(scales)
            res = pandas.Series(factors, index=scales)
        else:
            res = scaling_factors
        
        return res
    
    def normalized_samples(self, scaling_expr=None, scaling_factors=None):
        """Normalized samples by the scaling expr"""
        factors = self.get_scaling_factors(scaling_expr=scaling_expr, scaling_factors=scaling_factors)
        res = self / factors
        return res
    
    def kruskal_test(self, scaling_expr=None, scaling_factors=None):
        """Return a kruskal test on normalized samples"""
        samples = self.normalized_samples(scaling_expr=scaling_expr, scaling_factors=scaling_factors)
        samples = [samples[col] for col in self.columns]
        samples = [isample.dropna() for isample in samples]
        #~ print(samples)
        res = scipy.stats.kruskal(*samples)
        return res

    def _kruskal_statistic(self, params, scaling):
        """Return the kruskal statistic on normalized samples"""
        s_expr = ScalingExpr(scaling=scaling, *params)
        ktest = self.kruskal_test(scaling_expr=s_expr)
        res = ktest.statistic
        #~ print(params, res)

        return res

    def _kruskal_fit(self, scaling, **kwargs):
        """Fit the scaling using the kruskal statistic"""
        scaling = scaling_name(scaling)
        
        df0 = init_param_scaling(scaling, **kwargs)
        
        #~ print('')
        #~ print("start fit")
        facs = numpy.linspace(start=-1, stop=1, num=3, endpoint=True) # facs = [-1, 1] set num = 2
        #~ facs = [1]
        
        results = {}
        for ifit, fac in enumerate(facs):
            try:
                dfi = df0 * fac
                param0 = dfi.values
                pnames = dfi.index
                #optimization
                # ~ optim = scipy.optimize.fmin(self._kruskal_statistic, x0=param0, args=(scaling,), full_output=1, disp=0)
                optim = scipy.optimize.minimize(self._kruskal_statistic, x0=param0, method='Nelder-Mead', args=(scaling,))
                # ~ print(optim)
                # ~ raise
                # ~ dicres = {k : v for k, v in zip(['xopt', 'fopt', 'iter', 'funcalls', 'warnflag', "allvecs"], optim)}
                results[ifit] = optim
            except AssertionError as err:
                print(err)
                print(ifit, "optim failed")
            #~ print(dicres)
            #~ if scaling == KOUT_NAME:print(param0, "res:",dicres["fopt"], dicres["xopt"])
        assert len(results) >= 1, "all optim failed"

        fopts = {ifit : optm.fun for ifit, optm in sorted(results.items()) if optm.success}
        fopts = pandas.Series(fopts)
        idmin = fopts.idxmin()
        best = fopts.min()
        #~ print(fopts)
        assert fopts[idmin] == best, "pb code"

        res = results[idmin]
        # ~ print(res)
        if not res.success:
            print(res)
            # ~ print("optimization failed")
            # ~ print(optim)
            # ~ for k, v in res.items():
                # ~ print("{0:^20}    :    {1}".format(k, v))

        optpars = res.x

        dicpar = {}
        for parname, parvalue in zip(pnames, optpars):
            dicpar[parname] = parvalue
        
        # ~ res = d
        res['optpars'] = dicpar

        return res

    def fit_scaling_expr(self, method, scaling, **fitargs):
        """
        Fit the scaling expr on data
        method : moment or kruskal
        scaling : simple scaling or koutsoyiannis
        """
        scaling = scaling_name(scaling=scaling)
        #~ method  = fitmethod_name(method = method)

        if method == MOM:
            assert scaling == SIMSCALING_NAME, f"moment method not implemented for {scaling}"
            fit_moments = fitargs.pop('fit_moments')
    
            if isinstance(fit_moments, Number):
                fit_results = self.linear_regresion_logmoments_vs_logscales(moment_order=fit_moments)
                #~ eta_fitted        = self.lr_fitted["gradient"]
                
            elif isinstance(fit_moments, Iterable):
                fit_results = self.fit_linear_regression_k_vs_kq(moments=fit_moments)
            else:
                raise ValueError(f'Exp list or number got {fit_moments.__class__} : {fit_moments}')

            eta_fitted = fit_results["gradient"]
            fit_params = {ETA_NM : eta_fitted}

        elif method == KRUSKAL:
            argdic = dict(theta0=self.columns.min()/ 2, eta0=0.5)
            argdic.update(fitargs)
            fit_results = self._kruskal_fit(scaling=scaling, **argdic)
            fitargs.clear() #cheked
            fit_params = fit_results["optpars"]
        else:
            raise ValueError(f"Not implemented : {method}")

        assert not fitargs, f"dont know what to do with {fitargs}"

        fitted_scaling = ScalingExpr(scaling=scaling, **fit_params)
        
        res = self.__class__(data=self, columns=self.columns, scaling_expr=fitted_scaling, distribution=self.distribution)
        return res
        
    def fit_multi_simple_scaling(self, method, nscales=4, **fitargs):
        """
        Fit a broken simple scaling on data
        Use nscales to fit scaling exponents
        Return the obtaiend scaling factor, to be fit latter.
        """
        assert nscales % 2 == 0 and nscales >= 4
        indtoadd = (nscales - 2) / 2
        indtoadd = int(indtoadd)

        scales = self.columns
        slcs = [slice(i, i+nscales) for i in range(scales.size-nscales+1)]

        assert  len(slcs) == len(scales) - nscales + 1

        etas = []
        for slc in slcs:
            iscales = scales[slc]
            iscaling = ScalingData(self.loc[:,iscales])
            iscaling = iscaling.fit_scaling_expr(method=method, scaling=SIMSCALING_NAME, **fitargs)
            eta=iscaling.scaling_expr.eta
            etas.append(eta)
            # ~ print(slc, iscaling.columns, eta)
            # ~ print(self.columns)

        etas = [etas[0]] * indtoadd + etas + [etas[-1]] * indtoadd
        # ~ print(etas)

        scaling_factors = multi_simple_scaling(scales, etas)

        res = self.__class__(data=self, columns=scales, scaling_expr=None, distribution=self.distribution, scaling_factors=scaling_factors)
        
        return res
        
    
    def fit_distribution(self, model, method=LMOM, scales="all", **argfit):
        """Fit a distribution on normalized sample"""
        if scales == "all":
            values = self.normalized_samples().values.flatten()
            values = pandas.Series(values).dropna()
        elif isinstance(scales, Number):
            raise ValueError("Not implemented")
        else:
            raise ValueError(f"Dont know what to do with {scales}")

        #~ method = fitmethod_name(method = method)
        fitres = FitDist(x=values, model=model, method=method, **argfit)

        fdistr = fitres.distribution
        res = self.__class__(data=self, columns=self.columns, scaling_expr=self.scaling_expr,  scaling_factors=self.scaling_factors, distribution=fdistr)

        return res
        
    def scaled_distributions(self, scaling_expr=None, scaling_factors=None, distribution=None, par2scale=("loc", "scale")):
        """Return a scaled distirbution for each scale"""
        if distribution is None:
            d0 = self.distribution
        else:
            d0 = distribution

        factors = self.get_scaling_factors(scaling_expr=scaling_expr, scaling_factors=scaling_factors)
        distrs = [d0 .scale_distr(ratio=ifac, par2scale=par2scale) for ifac in factors.values]
        res = pandas.Series(distrs, index=factors.index)
        return res
        
        
    def nllhs(self, scaling_expr=None, distribution=None):
        """
        Return the neg log likelihood for each scale
        """
        distrs = self.scaled_distributions(scaling_expr=scaling_expr, distribution=distribution)
        
        scales = self.columns
        
        res = []
        for iscale in scales:
            dist = distrs.loc[iscale]
            vals = self[iscale]
            vals = vals.dropna()
            nllh = dist.nllh(vals)
            #~ print(iscale, vals.size)
            res.append(nllh)
        
        res = pandas.Series(res, index=scales)
        
        return res
        
    def nllh(self, scaling_expr=None, distribution=None):
        """Global negative log likelihood"""
        nllhs = self.nllhs(scaling_expr=scaling_expr, distribution=distribution)
        res = nllhs.sum()
        return res
        
        
    def _nllh(self, params, scaling, model):
        """neg log likelihood used for fit"""
        nparsca = npar_scaling(scaling)
        scapars = params[:nparsca]
        dispars = params[nparsca:]
        
        s_expr = ScalingExpr(scaling=scaling, *scapars)
        
        distr = Distribution(model, *dispars)
        
        res = self.nllh(scaling_expr=s_expr, distribution=distr)
        return res
        
    def _fit_min_nllh(self, scaling, distrinit, **kwargs):
        """Function used for minimizing the global nllh"""
        if isinstance(distrinit, str):
            scaind = numpy.abs((self.columns - 1)).argmin()
            sca0 = self.columns[scaind]
            ser0 = self[sca0].dropna()
            fit0 = FitDist(ser0, model=distrinit, method=LMOM)
            distrinit = fit0.distribution
            #~ print(sca0, ser0, distrinit)
        elif isinstance(distrinit, Distribution):
            pass
        else:
            raise ValueError("need a distribution or a name")

        
        scaling = scaling_name(scaling)
        df0 = init_param_scaling(scaling, **kwargs)
        param0 = df0.values

        param1 = distrinit.parargs()
        
        paraminit = list(param0) + list(param1[0]) + list(param1[1:])
        
        model = distrinit.name

        # ~ optim = scipy.optimize.fmin(self._nllh, x0=paraminit, args=(scaling, model), full_output=1, disp=0)
        res = scipy.optimize.minimize(self._nllh, x0=paraminit, args=(scaling, model), method='Nelder-Mead')
        # ~ assert 0, "test optim.minimize"
    
        # ~ res = {}

        # ~ for k, v in zip(['xopt', 'fopt', 'iter', 'funcalls', 'warnflag', "allvecs"], optim):
            # ~ res[k] = v
            
        if not res.success:
            print(res)

        optpars = res.x

        nparsca = npar_scaling(scaling)
        scapars = optpars[:nparsca]
        dispars = optpars[nparsca:]

        #distribution
        fdistr = Distribution(distrinit.name, *dispars)
        
        #scaling expr
        fexpr = ScalingExpr(scaling=scaling, *scapars)
        
        res["distr"] = fdistr
        res["scaexpr"] = fexpr

        return res
        
    def fit_global(self, scaling, distrinit):
        """Fitting by minimizing the global neg log likelihood"""
        optim = self._fit_min_nllh(scaling=scaling, distrinit=distrinit)
        fsca = optim["scaexpr"]
        fdistr = optim["distr"]
        
        res = self.__class__(data=self, columns=self.columns, scaling_expr=fsca, distribution=fdistr)

        return res
        
    def params(self):
        """Parameters in a dic"""
        res = dict(self.distribution.params())
        res.update(self.scaling_expr.params())
        return res
        
    def df_params(self, columns=None, index=0):
        """Parameters in a dataframe"""
        pars = self.params()
        
        if columns is None:
            columns = sorted(pars.keys())
        else:
            pars = {k : pars[k] for k in columns}
        
        if isinstance(index, Iterable):
            pass
        elif isinstance(index, Number):
            index = [index]
        else:
            raise ValueError("Index pb : {index}")
            
        res = pandas.DataFrame(pars, columns=columns, index=index)
        return res

    def get_param(self, key):
        """Return the value of one paramter"""
        sc_pars = self.scaling_expr.params()
        if key in sc_pars:
            res = sc_pars[key]
        else:
            res = self.distribution[key]
        return res

    def return_svd(self):
        """Return a comparison object between sample and model"""
        distrs = self.scaled_distributions()
        
        data = {}
        for scale in self.columns:
            sample = self[scale]
            distr = distrs.loc[scale]
            s_v_d = SampleDistr(sample_obj=sample, distr_obj=distr)
            data[scale] = s_v_d
            
        res = pandas.Series(data, index=self.columns)
            
        return res
        
    def get_colors(self, *colors):
        """Return a list of colors"""
        #~ print(colors)
        ncol = self.columns.size
        
        if not colors:
            res = ["k"] * ncol
        else:
            if len(colors) == 1:
                colors = colors[0]

            if isinstance(colors, str):
                res = get_colormap(name=colors, ncol=ncol, extend=False, fmt=list)
            elif isinstance(colors, Iterable):
                res = colors
            else:
                raise ValueError(f"Dont know what to do with : {colors}")
        
        assert len(res) == ncol, f"{res} has not the expected size ({ncol})"
        res = pandas.Series(res, index=self.columns)
        return res
        
    def plot_samples(self, normalize=False, **kwargs):
        """Plot the samples"""
        #~ argdic = {'marker' : "o", 'color' : "k", 'mec' : "k", 'mfc' : "0.5", 'ls' : ""}
        #~ argdic.update(kwargs)
        
        if normalize:
            dfdata = self.normalized_samples()
        else:
            dfdata = self
        
        res = {}
        for xscale in dfdata.columns:
            isample = dfdata[xscale]
            xs = [xscale] * isample.size
            ys = isample.values
            ys = numpy.sort(ys) #for marker and color attribution
            line = plot_multimarkers(x=xs, y=ys, **kwargs)#[0]
            res[xscale] = line
        
        return res

    def return_levels(self, t=10):
        """Return levels at the different scales for the given return period"""
        sdistrs = self.scaled_distributions()
        if isinstance(t, Number):
            ts = [t]
        else:
            assert isinstance(t, Iterable)
            ts = t
        
        ts = sorted(ts)
        
        rlevels = {itr : sdistrs.apply(lambda x: x.return_level(itr)) for itr in ts}
        res = pandas.DataFrame(rlevels, index=self.columns, columns=ts)
        
        if len(ts) == 1:
            res = pandas.Series(rlevels[t], name=t)
        
        return res

    def return_level_plot(self, *colors, normalized=False, **kwargs):
        """Return level plot for the different samples"""
        argdic = dict(plot_axes=1, mec='k', marker="o", ls="")
        argdic.update(kwargs)
        
        if normalized:
            samples = self.normalized_samples()
        else:
            samples = self
        
        coldf = self.get_colors(*colors)
        
        res = {}
        for xscale in self.columns:
            color = coldf.loc[xscale]
            label = f"{xscale:.0f}"
            #get sample
            isample = samples[xscale]
            isample = isample.dropna()
            isample = Sample(isample)
            line = isample.return_level_plot(label=label, mfc=color, **argdic)
            res[xscale] = line
            #~ print(xscale, color)

        return res

    def change_data(self, newdata):
        """To set other data. Return a new object with the same scaling_expr and distribution. Usefull for cal/val analysis."""
        res = self.__class__(data=newdata, scaling_expr=self.scaling_expr, distribution=self.distribution)
        return res
        
##======================================================================================================================##
##                IDF CLASS                                                                                             ##
##======================================================================================================================##

class IDF(ScalingData):
    """IDF class
    >>> rdm_data = random_sca_data(nval = 10, seed = 0)
    >>> sca_data = IDF(rdm_data)
    >>> isinstance(sca_data, IDF)
    True
    >>> sca_data.compute_moments_vs_scales(1, log = False)
    1     46.549154
    2     28.356503
    3     23.741704
    4     23.212911
    5     16.430299
    6     13.429643
    7     12.647328
    8     11.809209
    9      7.711743
    10     8.351314
    11     8.301167
    dtype: float64
    >>> sca_data.compute_moments_vs_scales(1, log = True)
    0.000000    1.667912
    0.301030    1.452653
    0.477121    1.375512
    0.602060    1.365730
    0.698970    1.215645
    0.778151    1.128064
    0.845098    1.101999
    0.903090    1.072221
    0.954243    0.887153
    1.000000    0.921755
    1.041393    0.919139
    dtype: float64
    >>> "{0:.2f}".format(sca_data.linear_regresion_logmoments_vs_logscales(1).gradient)
    '-0.75'
    >>> "{0:.2f}".format(sca_data.linear_regresion_logmoments_vs_logscales(2).gradient)
    '-1.49'
    >>> numpy.round(sca_data.compute_moment_scaling_function(range(1,5)), 2)
    array([-0.75, -1.49, -2.21, -2.91])
    >>> kqlr = sca_data.fit_linear_regression_k_vs_kq(range(1,5))
    >>> round(kqlr.intercept, 3)
    -0.043
    >>> round(kqlr.gradient, 2)
    -0.72
    >>> round(kqlr.rsquared, 0)
    1.0
    
    
    
    >>> sca_data1 = sca_data.fit_scaling_expr(method = MOM, scaling = SIMSCALING_NAME, fit_moments = 1).fit_distribution( model='gev')
    >>> sca_data2 = sca_data.fit_scaling_expr(method = MOM, scaling = SIMSCALING_NAME, fit_moments = [1,2]).fit_distribution( model='gev')
    >>> sca_data3 = sca_data.fit_scaling_expr(method = KRUSKAL, scaling = SIMSCALING_NAME).fit_distribution( model='gev')
    >>> sca_datak = sca_data.fit_scaling_expr(method = KRUSKAL, scaling = KOUT_NAME).fit_distribution( model='gev')
    
    >>> sca_data1.get_scaling_factors().round(2)
    1.0     1.00
    2.0     0.59
    3.0     0.44
    4.0     0.35
    5.0     0.30
    6.0     0.26
    7.0     0.23
    8.0     0.21
    9.0     0.19
    10.0    0.18
    11.0    0.16
    dtype: float64
    
    
    #~ >>> sca_datag = sca_data.fit_global(scaling = 's', distrinit = sca_data1.distribution)
    
    >>> isinstance(sca_data, IDF)
    True
    
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.scaling_expr.eta,2))
    -0.75
    -0.74
    -0.77
    
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.distribution['loc'],0))
    44.0
    43.0
    45.0
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.distribution['scale'],0))
    13.0
    13.0
    13.0
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.distribution['c'],3))
    -0.074
    -0.068
    -0.08
    
    >>> print(sca_datak.scaling_expr)
    KOUTSOYIANNIS
    eta     -1.077246
    theta    1.335289
    dtype: float64
    >>> ns = sca_data1.normalized_samples()
    >>> ns[1].sort_values()
    2    20.309984
    5    37.528194
    4    40.534874
    6    42.411332
    3    47.336340
    7    49.092162
    8    51.183723
    0    52.023072
    9    57.385867
    1    67.685996
    Name: 1, dtype: float64

    >>> ns[11].sort_values()
    6    26.365692
    1    27.548774
    0    37.707994
    4    46.673466
    2    48.306823
    5    49.174676
    7    49.251612
    3    60.866138
    9    71.583000
    8    88.370478
    Name: 11, dtype: float64
    
    
    >>> sd = sca_data1.scaled_distributions()
    >>> print(sd.loc[1])
    name     genextreme
    a          -13.5758
    b               inf
    c        -0.0736606
    loc         43.5634
    scale       12.8102

    >>> print(sd.loc[11])
    name     genextreme
    a          -13.5758
    b               inf
    c        -0.0736606
    loc         7.14891
    scale       2.10221


    >>> sca_data1.to_montana_param(t = 10)
                    a         b
    Tr=10yr  74.91804 -0.753684

    >>> sca_data1.to_montana_df([2,10, 100])
                       a         b
    Tr=2yr     48.322422 -0.753684
    Tr=10yr    74.918040 -0.753684
    Tr=100yr  113.706432 -0.753684
    >>> sca_data1.df_params()
              c       eta        loc      scale
    0 -0.073661 -0.753684  43.563356  12.810227
    >>> sca_data1.get_colors('jet')
    1     #000080
    2     #0000f6
    3     #004cff
    4     #00b0ff
    5     #29ffce
    6     #7dff7a
    7     #ceff29
    8     #ffc800
    9     #ff6800
    10    #f10800
    11    #800000
    dtype: object
    >>> msvd = sca_data1.return_svd()
    
    >>> sca_data1.return_levels(t = 10)
    1.0     74.918040
    2.0     44.432912
    3.0     32.733130
    4.0     26.352581
    5.0     22.273250
    6.0     19.413593
    7.0     17.284195
    8.0     15.629372
    9.0     14.301733
    10.0    13.209974
    11.0    12.294332
    Name: 10, dtype: float64

    >>> fig = pyplot.figure()
    >>> l = sca_data.plot_moments_vs_scales(1)
    >>> fig = pyplot.figure()
    >>> obs = sca_data.plot_moment_scaling_function(moments = range(1,5))
    >>> mod = sca_data.plot_simple_scaling(range(1,5), "q1")
    >>> fig.show()
    
    >>> fig = pyplot.figure()
    >>> lines = sca_data1.plot_samples()
    >>> fig.show()
    
    
    >>> fig = pyplot.figure()
    >>> lines = sca_data1.return_level_plot('jet', normalized = True)
    >>> fig.show()
    
       
    >>> valdata = sca_data1 * 2
    >>> valdata = sca_data1.change_data(valdata)
    >>> print(valdata.distribution)
    name     genextreme
    a          -13.5758
    b               inf
    c        -0.0736606
    loc         43.5634
    scale       12.8102
    
    >>> print(valdata.scaling_expr)
    SIMPLE_SCALING
    eta   -0.753684
    dtype: float64

    >>> sca_data1.min()
    1     20.309984
    2     18.249808
    3     14.343912
    4      9.072480
    5     11.721552
    6      8.019187
    7      7.626059
    8      5.518659
    9      4.976065
    10     4.912763
    11     4.326709
    dtype: float64
    
    
    >>> sca_data1.max()
    1     67.685996
    2     35.293844
    3     36.182194
    4     38.858086
    5     25.375197
    6     20.627728
    7     25.184114
    8     21.154252
    9      9.601431
    10    11.356288
    11    14.501928
    dtype: float64

    
    >>> valdata.min()
    1     40.619968
    2     36.499615
    3     28.687824
    4     18.144960
    5     23.443103
    6     16.038373
    7     15.252117
    8     11.037317
    9      9.952131
    10     9.825525
    11     8.653418
    dtype: float64
    
    >>> valdata.max()
    1     135.371991
    2      70.587689
    3      72.364389
    4      77.716172
    5      50.750395
    6      41.255456
    7      50.368227
    8      42.308504
    9      19.202862
    10     22.712576
    11     29.003855
    dtype: float64
    
    #>>> idfrl = sca_data1.plot_idf_return_level()
    
    >>> rdm_data = random_sca_data(scales=numpy.arange(0.1,2,0.1),nval = 10, seed = 0)
    >>> sca_data = IDF(rdm_data)
    >>> sca_data.mean().round()
    0.1    294.0
    0.2    179.0
    0.3    150.0
    0.4    146.0
    0.5    104.0
    0.6     85.0
    0.7     80.0
    0.8     75.0
    0.9     49.0
    1.0     53.0
    1.1     52.0
    1.2     52.0
    1.3     60.0
    1.4     46.0
    1.5     35.0
    1.6     36.0
    1.7     34.0
    1.8     42.0
    1.9     36.0
    dtype: float64
    
    
    >>> sca_data1 = sca_data.fit_scaling_expr(method = MOM, scaling = SIMSCALING_NAME, fit_moments = 1).fit_distribution( model='gev')
    >>> sca_data2 = sca_data.fit_scaling_expr(method = MOM, scaling = SIMSCALING_NAME, fit_moments = [1,2]).fit_distribution( model='gev')
    >>> sca_data3 = sca_data.fit_scaling_expr(method = KRUSKAL, scaling = SIMSCALING_NAME).fit_distribution( model='gev')
    >>> sca_datak = sca_data.fit_scaling_expr(method = KRUSKAL, scaling = KOUT_NAME).fit_distribution( model='gev')
    
    >>> sca_data1.get_scaling_factors().round(2)
    0.1    5.68
    0.2    3.37
    0.3    2.48
    0.4    2.00
    0.5    1.69
    0.6    1.47
    0.7    1.31
    0.8    1.18
    0.9    1.08
    1.0    1.00
    1.1    0.93
    1.2    0.87
    1.3    0.82
    1.4    0.78
    1.5    0.74
    1.6    0.70
    1.7    0.67
    1.8    0.64
    1.9    0.62
    dtype: float64
    
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.scaling_expr.eta,2))
    -0.75
    -0.74
    -0.76

    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.distribution['loc'], 0))
    48.0
    48.0
    48.0
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.distribution['scale'], 0))
    15.0
    15.0
    15.0
    >>> for fit in (sca_data1,sca_data2,sca_data3):print(round(fit.distribution['c'], 3))
    -0.086
    -0.079
    -0.09

    """

    def __init__(self, data, scaling_expr=None, distribution=None, **kwargs):
        """ Class initialiser """
        ScalingData.__init__(self, data=data, scaling_expr=scaling_expr, distribution=distribution, **kwargs)
        
    def to_montana_param(self, t):
        """
        return the montana param for the given return period t
        """
        assert self.scaling_expr.scaling == SIMSCALING_NAME
        distr = self.distribution
        rlev = distr.return_level(t=t)
        eta = self.scaling_expr.eta
        rlkey = f"Tr={t}yr"
        #~ etkey = ETA_NM
        df = {"a" : [rlev], "b" : [eta]}
        res = pandas.DataFrame(df, columns=sorted(df.keys()), index=[rlkey])
        return res
        
    def to_montana_df(self, ts):
        """
        Montana parameters
        """
        dfs = [self.to_montana_param(t=it) for it in sorted(ts)]
        res = pandas.concat(dfs)
        return res

        

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='IDF module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    parser.add_argument('--multisimplescaling', default=False, action='store_true')
    #~ parser.add_argument('--pickle', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        
        close_all()
        print("example")

        #~ scles = 
        eta_th = 0.8
        
        mod = 'gev'


        scaexpr = ScalingExpr(scaling="s", eta=eta_th)
        #~ scaexpr = ScalingExpr(scaling = "k", eta = eta_th, theta = -0.5)
        
        datas = random_sca_data(scaling_expr=scaexpr)
        
        
        
        
        fig = pyplot.figure()
        datas.return_level_plot("viridis", normalized=True)
        datas.distribution.return_level_plot()
        
        fig.show()

        print(datas.normalized_samples(scaexpr))
        print(datas.kruskal_test(scaexpr))
        #~ raise 0
        #~ globf=scadata.min_nllh(scaling = 'simple', distrinit = Distribution(mod, 0.1, loc = 40, scale = 8))
        
        fss = datas.fit_scaling_expr(scaling="simple", method="mom", fit_moments=1)
        svm = fss.compute_moments_vs_scales(1)
        print(svm)
        lrr = fss.linear_regresion_logmoments_vs_logscales(1)
        print(lrr)
        pyplot.figure()
        fss.plot_moments_vs_scales(1)
        print(fss.scaling_expr.scaling, fss.scaling_expr.eta, fss.scaling_expr(2))

        fss2 = datas.fit_scaling_expr(scaling="simple", method="mom", fit_moments=[1, 2, 3, 4])
        print(fss.scaling_expr.eta, fss2.scaling_expr.eta)
        
        fkrus = datas.fit_scaling_expr(scaling="simple", method="kruskal")
        #~ fkrus._kruskal_statistic(0.8)
        #~ fkrus._kruskal_fit()

        fkruskout = datas.fit_scaling_expr(scaling="kout", method="kruskal")

        #~ scadata = IDF(datas, columns = scles)
        fglobs = datas.fit_global('simple', 'gev')

        for nm, scaf in zip(['ssmom1', "ssmoms", "sskrus", "koutkrus", "glob"], [fss, fss2, fkrus, fkruskout, fglobs]):
            fgev = scaf.fit_distribution(model=mod, method=MLE)
            print(nm, fgev.nllh(), fgev.distribution['loc'], fgev.distribution["scale"], fgev.distribution['c'])
                
        show_all()

        
        fglobs.return_svd()
        #~ fglobs.set_colors(colors = "gnuplot")
        
        pyplot.figure()      
        fglobs.plot_samples()
        
        pyplot.figure()
        fglobs.return_level_plot()
        
        #~ pyplot.figure()
        #~ fglobs.plot_idf_return_level()
        
        pyplot.figure()
        fglobs.return_level_plot("gnuplot", normalized=True)
        fglobs.distribution.return_level_plot(color='k')
        
        show_all()
        
        
        close_all()

        fpc = os.path.join(os.getenv("HOME"), "fidf.pic")
        with open(fpc, "wb") as picw:
            #~ pickle.dump(file = picw, obj = fglobs)
            dill.dump(file=picw, obj=fglobs)

        with open(fpc, "rb") as picr:
            #~ fidf2 = pickle.load(file = picr)
            fidf2 = dill.load(file=picr)
            
        print(fidf2.scaling_expr)
        print(fidf2.distribution)
        os.remove(fpc)
        
        svds = fglobs.return_svd()


    #+++++++++++++++++++++++++++++++#
    #    MULTISIMPLESCALING         #
    #+++++++++++++++++++++++++++++++#
    
    if opts.multisimplescaling:

        eta_th = 0.8
                
        mod = 'gev'


        scaexpr = ScalingExpr(scaling="s", eta=eta_th)
        #~ scaexpr = ScalingExpr(scaling = "k", eta = eta_th, theta = -0.5)

        datas = random_sca_data(scaling_expr=scaexpr)

        # ~ self = random_sca_data(scaling_expr=scaexpr)
        # ~ 

        # ~ method= KRUSKAL#,  **fitargs
        # ~ fitargs = {}
        

        ns = 11
        ne = ns-1
        sca = [0.5*2**i for i in range(ns)]
        eta_0 = numpy.linspace(-0.1, -0.9, num=int(ne/2))
        ets = sorted(eta_0) + sorted(eta_0, reverse=True)

        facs_ = multi_simple_scaling(scales=sca,etas=ets)

        datas = random_sca_data(scaling_factors=facs_, scales=sca)


        idf1 = datas.fit_scaling_expr(method=MOM, scaling=SIMSCALING_NAME, fit_moments=4)
        idf1 = idf1.fit_distribution(model="gev")

        idftrue = datas.fit_multi_simple_scaling(method=KRUSKAL)
        idftrue = idftrue.fit_distribution(model="gev")


        fig = pyplot.figure()
        ax = fig.gca()

        pyplot.plot(facs_.values, idf1.get_scaling_factors().values, label="simple scaling", marker="o", ls="")
        pyplot.plot(facs_.values, idftrue.get_scaling_factors().values, label="multi_simple_scaling", marker="o", ls="")
        plot_one_one()
        pyplot.legend()
        ax.set(xlabel="True scaling factors", ylabel="Estimated")

        show_all()
        
        
        fig = pyplot.figure()
        ax = fig.gca()
        
        idftrue.plot_samples()
        idftrue.return_levels(2).plot()
        idftrue.return_levels(10).plot()
        pyplot.loglog()
        pyplot.grid(1)
        
        show_all()
