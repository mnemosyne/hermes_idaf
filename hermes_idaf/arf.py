"""
Areal Reduction Factor for rainfall
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import doctest
import argparse

import numpy
import pandas
from matplotlib import pyplot

from hermes_idaf.idf import ETA_NM

try:
    from japet_misc import show_all, plot_colorbar, linvector
    from spatialobj import ZGrid
except ImportError:
    print("Connaot import spatialobj or functions")
    

##======================================================================================================================##
##                CONSTNATNS                                                                                            ##
##======================================================================================================================##

DE_MICHELE_NAME = "demichele"
OMEGA_NM = "omega"
A_NM = "a"
B_NM = "b"

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


#~ def de_michele_arf():
    #~ """
    #~ Function doc
    #~ >>> 'afarie'
    #~ """
    #~ res = (1 + omega * ((area ** a) / (duration ** b)))**(eta / b)
    #~ return res

class ArealScalingExpr:
    """ Class doc
    >>> arf = ArealScalingExpr(eta=-0.90, a=0.165, b=0.156, omega=0.026)
    >>> arf(duration=1,area=0)
    1.0
    >>> arf(duration=24,area=0)
    1.0
    >>> format(arf(duration=24,area=1), '.2f')
    '0.91'
    >>> format(arf(duration=1,area=1), '.2f')
    '0.86'
    
    >>> format(arf(duration=24,area=100), '.2f')
    '0.83'
    >>> format(arf(duration=1,area=100), '.2f')
    '0.73'
    
    >>> format(arf(duration=24,area=10000), '.2f')
    '0.67'
    
    >>> format(arf(duration=1,area=10000), '.2f')
    '0.52'
    
    >>> print(arf)
    DEMICHELE
    a        0.165
    b        0.156
    eta     -0.900
    omega    0.026
    dtype: float64

    >>> zg = arf.to_zgrid(durations=(1,2,12,24), areas=(0,1,100,1000))
    >>> print(zg.round(2))
          0     1     100   1000
    1.0    1.0  0.86  0.73  0.64
    2.0    1.0  0.88  0.76  0.67
    12.0   1.0  0.90  0.81  0.73
    24.0   1.0  0.91  0.83  0.76
    """
    
    def __init__(self, *args, scaling=DE_MICHELE_NAME, **kwargs): 
        """ Class initialiser """
        #~ print(arg, kwargs)
        assert scaling in (DE_MICHELE_NAME, )
        self.scaling = scaling

        self.omega = None
        self.a = None
        self.b = None
        self.eta = None

        dic_pars = {}
        p_names = [OMEGA_NM, A_NM, B_NM, ETA_NM]

        if kwargs:
            for kpar, pval in kwargs.items():
                assert kpar in p_names, f"{kpar} not in params"
                i2pop = p_names.index(kpar)
                p_names.pop(i2pop)
                # ~ print(rmd)
                dic_pars[kpar] = pval
        
        if args:
            if len(args) == 1:
                vals = list(args[0])
            else:
                vals = list(args)
            
            assert len(vals) == len(p_names)
            k2pops = []
            for kpar, pval in zip(p_names, vals):
                dic_pars[kpar] = pval
                k2pops.append(kpar)
            
            for kpar in k2pops:
                i2pop = p_names.index(kpar)
                p_names.pop(i2pop)


        dic_pars = {k : float(v) for k, v in dic_pars.items()}
        
        assert not p_names, "uncorrect number of arguments : {0}, {1}\nExpect {2}".format(args, kwargs, p_names)

        for kp, pv in dic_pars.items():
            # ~ print(kp, pv)
            setattr(self, kp, pv)


        assert all(i is not None for i in (self.omega, self.a, self.b, self.eta))

            
        #~ assert len(kwargs) == 0, "Dont know what to do with {0}".format(kwargs)
        #~ assert len(arg) == 0, "Dont know what to do with {0}".format(arg)

    def __str__(self):
        """Object representation"""
        hd = self.scaling.upper()
        df = self.params()
        
        res = "\n".join([hd, str(df)])
        return res
        

    def params(self):
        """Return a pandas.Series containing parameters"""
        cols = [k for k in (OMEGA_NM, A_NM, B_NM, ETA_NM) if hasattr(self, k)]
        
        res = {k : getattr(self, k) for k in cols}
        res = pandas.Series(res, index=cols)
        res = res.sort_index()
        return res
        

    def new_params(self, **kwargs):
        """Return a new expr with different parameters"""
        res = ArealScalingExpr(scaling=self.scaling, **kwargs)
        return res

    def __call__(self, duration, area):
        """Return scaled ratio for given scales"""
        res = (1 + self.omega * ((area ** self.a) / (duration ** self.b)))**(self.eta / self.b)
        return res
        
    def to_zgrid(self, durations=numpy.arange(1, 25), areas=numpy.arange(1, 2501, 5)):
        """return a DataFrame (ZGrid) containing the arf values for different spatio-temporal scales"""
        # ~ _a = "areas"
        # ~ _d = 'durations'
        # ~ dicargs = {_d : numpy.arange(1, 25), _a : numpy.arange(1, 2501, 5)}
        # ~ dicargs.update(**kwargs)
        
        # ~ areas = dicargs[_a]
        # ~ durations = dicargs[_d]
        durations = numpy.array(durations, dtype=float)
        
        objs = [pandas.Series(self(area=ar, duration=durations),index = durations, name=ar) for ar in areas]
        obj = pandas.concat(objs, axis = 1)
        res = ZGrid(obj, index = durations, columns = areas)
        
        return res

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##




##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPIONTS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--example', default=False, action='store_true')
    
    parser.add_argument('--eta', default=-0.9, type=float)
    parser.add_argument('--omega', default=0.026, type=float)
    parser.add_argument('--a', default=1.6, type=float)
    parser.add_argument('--b', default=1.6, type=float)
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    if opts.example:
        arf=ArealScalingExpr(opts.omega, opts.a, opts.b, opts.eta)
        #~ arf=ArealScalingExpr(0.01, 1.6, 1.6,eta=-0.9)
        #~ arf=ArealScalingExpr(0.026, 1, 1,eta=-0.9)

        title = arf.params().to_string().replace('\n', ' | ')
        
        arfgrid = arf.to_zgrid()
        
        bds = linvector(0, 1, 0.05)
        lvls = linvector(0.1, 1, 0.1)
        
        #~ close_all()
        fig = pyplot.figure()
        cax = pyplot.axes([0.92, 0.1, 0.05, 0.8])
        ax = pyplot.axes([0.1, 0.1, 0.8, 0.8])
        cb = plot_colorbar(bds, cmap="viridis", cax=cax)
        
        arfgrid.contourf(colorbar=cb)
        arfgrid.contour(levels = lvls)
        
        pyplot.grid(1)
        #~ xlim = ax.get_xlim()
        #~ ylim = ax.get_ylim()
        pyplot.loglog()
        ax.set(xlabel="Area (km2)", ylabel="Duration (h)", ylim=(1,24),xlim=(1,2500), title=title)#xlim=xlim, ylim=ylim)
        
        show_all()
